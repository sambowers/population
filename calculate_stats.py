import numpy as np
from osgeo import gdal
import biota.mask
from osgeo import gdal
import scipy.ndimage.filters

import pdb

class DummyTile(object):
    """
    This class allows maskShapefile to understand a standard geoTiff
    """
        
    def __init__(self, geoTiff):
        """
        Loads data and metadata from a geoTiff to mimic an ALOS tile
        """
                
        from osgeo import gdal
        
        ds = gdal.Open(geoTiff)
        
        self.geo_t = ds.GetGeoTransform()
        self.proj = ds.GetProjection()
        self.xSize = ds.RasterXSize
        self.ySize = ds.RasterYSize
        self.xRes = 1
        self.yRes = 1
        

def reproject(data, ds, tile_dest, resampling = 0):
    '''
    '''
    
    # Create input file (GDAL format)
    gdal_driver = gdal.GetDriverByName('MEM')
    ds_source = gdal_driver.Create('', ds.RasterXSize, ds.RasterYSize, 1, 3)
    ds_source.GetRasterBand(1).WriteArray(data)
    ds_source.SetGeoTransform(ds.GetGeoTransform())
    ds_source.SetProjection(ds.GetProjection())
    

    # Create output file matching ALOS tile
    gdal_driver = gdal.GetDriverByName('MEM')
    ds_dest = gdal_driver.Create('', tile_dest.xSize, tile_dest.ySize, 1, 3)
    ds_dest.SetGeoTransform(tile_dest.geo_t)
    ds_dest.SetProjection(tile_dest.proj)
       
    # Reproject input GeoTiff to match the ALOS tile
    gdal.ReprojectImage(ds_source, ds_dest, ds.GetProjection(), tile_dest.proj, resampling)
    
    # Load resampled image into memory
    resampled = ds_dest.GetRasterBand(1).ReadAsArray()
    
    return resampled



population_data = 'DATA/glup00ag.bil'
city_vector = 'DATA/global_urban_extent_polygons_v1.01.shp'
city_raster = 'DATA/urban_extent.tif'
avitabile_data = '/home/sbowers3/guasha/sam_bowers/IIED/DATA/avitabile/Avitabile_AGB_Map.tif'

pc = 75. # Percentile for filter
box = 11
bm_high = 75.
bm_low = 5.


### Population ###

# Load global population (2000)
ds = gdal.Open(population_data,0)
population = np.ma.array(ds.ReadAsArray(), mask = False)

# Mask out areas outside tropics (Cancer - Capricorn)
tropic = 23.5

geo_t = ds.GetGeoTransform()
lonrange = np.linspace(geo_t[0], geo_t[0] + geo_t[1] * ds.RasterXSize, ds.RasterXSize)
latrange = np.linspace(geo_t[3], geo_t[3] + geo_t[5] * ds.RasterYSize, ds.RasterYSize)
lons, lats = np.meshgrid(lonrange, latrange)

# Obliterate the extra-tropical population
population.mask[np.logical_or(lats > tropic, lats < -tropic)] = True


tropical_population = np.sum(population)
total_population = np.sum(population.data)
print 'Total tropical population: %s'%tropical_population
print 'Total world population: %s'%total_population

# Load urban population
#urban_mask = biota.mask.maskShapefile(DummyTile(population_data), city_vector)
ds = gdal.Open(city_raster, 0)
urban_mask = ds.ReadAsArray().astype(np.bool)

urban_tropical_population = np.sum(population[urban_mask])
urban_population = np.sum(population.data[urban_mask])

print 'Urban tropical population: %s'%urban_tropical_population
print 'Urban world population: %s'%urban_population

# Obliterate urban population
population.mask = np.logical_or(urban_mask, population.mask)


### AGB ###

## Load AGB
ds = gdal.Open(avitabile_data, 0)
agb = ds.ReadAsArray()
water = agb < 0.
agb[water] = 0 # Get rid of the sea
agb_pc = scipy.ndimage.filters.percentile_filter(agb, pc, size = (box,box))


# Reproject AGB, and AGB_pc
agb_area = reproject(np.ones_like(agb), ds, DummyTile(population_data), resampling = gdal.GRA_NearestNeighbour)
agb_area = agb_area == 1
agb = reproject(agb, ds, DummyTile(population_data), resampling = gdal.GRA_NearestNeighbour)
agb_pc = reproject(agb_pc, ds, DummyTile(population_data), resampling = gdal.GRA_NearestNeighbour)
water = reproject(water, ds, DummyTile(population_data), resampling = gdal.GRA_NearestNeighbour)
water = water == 1


# Calculate locations of 'forest', 'woodland', and 'desert'
dense_forest = agb >= bm_high
woodland = np.logical_and(agb < bm_high, agb >= bm_low)
desert = np.logical_and(agb < bm_low, water == False)

dense_forest_pc = agb_pc >= bm_high
woodland_pc = np.logical_and(agb_pc < bm_high, agb_pc >= bm_low)
desert_pc = np.logical_and(agb_pc < bm_low, water == False)

# Calculate population by class
forest_population = population[dense_forest].sum()
desert_population = population[desert].sum()
woodland_population = population[woodland].sum()

forest_population_pc = population[dense_forest_pc].sum()
woodland_population_pc = population[woodland_pc].sum()
desert_population_pc = population[desert_pc].sum()


print 'Forest population (pc): %s'%forest_population_pc
print 'Woodland population (pc): %s'%woodland_population_pc
print 'Desert population (pc): %s'%desert_population_pc


# Write output maps
biota.IO.outputGeoTiff(agb_pc, 'AGB_percentile.tif', DummyTile(population_data).geo_t, DummyTile(population_data).proj, dtype = 6, nodata = None)
biota.IO.outputGeoTiff(agb, 'AGB.tif', DummyTile(population_data).geo_t, DummyTile(population_data).proj, dtype = 6, nodata = None)
biota.IO.outputGeoTiff(np.logical_or(lats > tropic, lats < -tropic)*1, 'tropics.tif', DummyTile(population_data).geo_t, DummyTile(population_data).proj, dtype = 1, nodata = None)

# Output a land cover map (approx)

landscape_lc = np.zeros_like(agb, dtype = np.int)
landscape_lc[dense_forest_pc] = 2
landscape_lc[woodland_pc] = 3
landscape_lc[desert_pc] = 4
landscape_lc[urban_mask] = 5
landscape_lc[water] = 1
landscape_lc[agb_area == False] = 0

biota.IO.outputGeoTiff(landscape_lc, 'landscape_lc.tif', DummyTile(population_data).geo_t, DummyTile(population_data).proj, dtype = 1, nodata = 0)


### Output statistics ###

import csv
with open('population_summary.csv', 'wb') as csvfile:
    writer = csv.writer(csvfile, delimiter = ',')
    writer.writerow(['POPULATION', 'COUNT', 'DESCRIPTION'])
    writer.writerow(['Global', int(total_population), 'For year 2000'])
    writer.writerow(['Tropical', int(tropical_population), 'Total population +/- 23.5 degrees from equator'])
    writer.writerow(['Urban population' , int(total_population - urban_population), 'Global urban population (based on 1995 city extents)'])
    writer.writerow(['Rural population' , int(urban_population), 'Global urban population (based on 1995 city extents)'])
    writer.writerow(['Urban tropical population' , int(urban_tropical_population), 'Global urban population +/- 2.5 degrees from equator (based on 1995 city extents)'])
    writer.writerow(['Rural tropical population' , int(tropical_population - urban_tropical_population), 'Global urban population +/- 2.5 degrees from equator (based on 1995 city extents)'])
    writer.writerow(['Rural tropical forest population', int(forest_population_pc) , 'Rural populations in forested areas.'])
    writer.writerow(['Rural woodland/savanna population', int(woodland_population_pc) , 'Rural populations in woodland/savannah areas.'])
    writer.writerow(['Rural unvegetated population', int(desert_population_pc) , 'Rural populations in unvegetated areas.'])

